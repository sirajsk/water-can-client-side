// import React, { useState, useEffect } from 'react';
// import { Paper, Grid, Table, TableHead, TableBody, TableRow, TableCell } from '@mui/material';
// import orderService from '../../services/orderService';
// import manufacturerService from '../../services/manufacturerService';
// import productService from '../../services/productService';

// const Orders = () => {
//   const [data, setData] = useState(null);
//   const [manufacturers, setManufacturers] = useState({});
//   const [products, setProducts] = useState({});

//   useEffect(() => {
//     const fetchData = async () => {
//       try {
//         // Fetch orders
//         const id = localStorage.getItem("id");
//         const response = await orderService.getOrders(id);
//         if (!response.ok) {
//           throw new Error('Failed to fetch data');
//         }
//         const result = await response.json();
//         setData(result);

//         // Fetch manufacturers
//         const manufacturerResponse = await manufacturerService.getAllManufacturers();
//         if (!manufacturerResponse.ok) {
//           throw new Error('Failed to fetch manufacturers');
//         }
//         const manufacturerData = await manufacturerResponse.json();
//         const manufacturerMap = {};
//         manufacturerData.forEach(manufacturer => {
//           manufacturerMap[manufacturer._id] = manufacturer.firstName + " " + manufacturer.lastName;
//         });
//         setManufacturers(manufacturerMap);

//         // Fetch products
//         const productResponse = await productService.getAllProducts();
//         if (!productResponse.ok) {
//           throw new Error('Failed to fetch products');
//         }
//         const productsData = await productResponse.json();
//         const productsMap = {};
//         productsData.forEach(product => {
//           productsMap[product._id] = {
//             name: product.productName,
//             weight: product.productWeight
//           };
//         });
//         setProducts(productsMap);
//       } catch (error) {
//         console.error('Error fetching data:', error.message);
//       }
//     };

//     fetchData();
//   }, []);

//   return (
//     <div>
//       <Grid item xs={12}>
//         <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
//           <Table size="small">
//             <TableHead>
//               <TableRow>
//                 <TableCell>Product Name</TableCell>
//                 <TableCell>Product Weight(in Liters)</TableCell>
//                 <TableCell>Manufacture Name</TableCell>
//                 <TableCell>Product Price(Rs.)</TableCell>
//                 <TableCell>Quantity</TableCell>
//                 <TableCell>Amount Paid</TableCell>
//                 <TableCell>Date Of Order</TableCell>
//                 <TableCell>Delivery Date</TableCell>
//               </TableRow>
//             </TableHead>
//             <TableBody>
//               {data && data.map(row => (
//                 <TableRow key={row._id}>
//                   <TableCell>{products[row.productId]?.name}</TableCell>
//                   <TableCell>{products[row.productId]?.weight}</TableCell>
//                   <TableCell>{manufacturers[row.manufactureId]}</TableCell>
//                   <TableCell>{row.amount}</TableCell>
//                   <TableCell>{row.quantity}</TableCell>
//                   <TableCell>{row.totalAmount}</TableCell>
//                   <TableCell>{row.createdAt}</TableCell>
//                   <TableCell>With in 48 hours</TableCell>
//                 </TableRow>
//               ))}
//             </TableBody>
//           </Table>
//         </Paper>
//       </Grid>
//     </div>
//   );
// };

// export default Orders;

import React, { useState, useEffect } from 'react';
import Link from '@mui/material/Link';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import orderService from '../../services/orderService';
import productService from '../../services/productService';
import userService from '../../services/userService';
import vendorService from '../../services/vendorService';
// Generate Order Data
function createData(id, date, name, shipTo, quantity, amount) {
  return { id, date, name, shipTo, quantity, amount };
}

// Helper function to parse and format the date
function formatCreatedAtDate(createdAt) {
  const date = new Date(createdAt);
  return date.toLocaleDateString(); // Adjust the formatting as needed
}

export default function Orders() {
  const id = localStorage.getItem("id");
  const [orders, setOrders] = useState([]);
  const [users, setUsers] = useState({});
  const [products, setProducts] = useState({});
  useEffect(() => {
    fetchOrders();
  }, []);

  const fetchOrders = async () => {
    try {
      const response = await vendorService.getOrders(id);
      if (!response.ok) {
        throw new Error('Failed to fetch orders');
      }
      const data = await response.json();
      setOrders(data);
      const productResponse = await productService.getAllProducts();
            if (!productResponse.ok) {
              throw new Error('Failed to fetch manufacturers');
            }
            const productsData = await productResponse.json();
            const productsMap = {};
            productsData.forEach(products => {
              productsMap[products._id] = products.productName;
            });
           
            setProducts(productsMap);
      // *****************
            const userResponse = await userService.getAllUsers();
            if (!userResponse.ok) {
              throw new Error('Failed to fetch manufacturers');
            }
            const userData = await userResponse.json();
             const userMap = {};
             userData.forEach(users => {
               userMap[users._id] = users.firstName;
             });
            setUsers(userMap);

    } catch (error) {
      console.error(error.message);
    }
  };

  return (
    <Grid item xs={12}>
      <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
        <React.Fragment>
          Recent Orders
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell>Date</TableCell>
                <TableCell>Product Name</TableCell>
                <TableCell>Ship To</TableCell>
                <TableCell>Quantity</TableCell>
                <TableCell align="right">Total Amount</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {orders && orders.map((row) => (
                <TableRow key={row.id}>
                  <TableCell>{formatCreatedAtDate(row.createdAt)}</TableCell>
                  <TableCell>{products[row.productId]}</TableCell>
                  <TableCell>{users[row.userId]}</TableCell>
                  <TableCell>{row.quantity}</TableCell>
                  <TableCell align="right">{`Rs.${row.amount}`}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          {/* <Link color="primary" href="#" onClick={preventDefault} sx={{ mt: 3 }}>
            See more orders
          </Link> */}
        </React.Fragment>
      </Paper>
    </Grid>
  );
}
