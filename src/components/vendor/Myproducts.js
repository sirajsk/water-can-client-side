

// UpdateForm.js
import * as React from "react";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom';
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import manufacturerService from "../../services/manufacturerService";
import productService from "../../services/productService";
import EditIcon from '@mui/icons-material/Edit';
import orderService from "../../services/orderService";
import { Select, MenuItem } from '@mui/material';
const theme = createTheme();
const Myproducts = ({ initialValues }) => {
  const navigate = useNavigate();
const number=initialValues[0].pNumber;
const userId= localStorage.getItem("id");
  const handleSubmit = async (values, actions) => {
       try {

        //orderService.placeOrder(values,userId);
          orderService.placeOrderV({
          manufactureId: values.manufactureId,
          userId: userId,
          vendorId:userId,
          productId: values._id,
          quantity: values.pNumber,
          productName: values.productName,
          productWeight: values.productWeight,
          productPrice: values.productPrice,
          imageUrl: values.imageUrl
        });
      toast.success("Your Product Added Successfully!!! ");
      navigate('/vendor');
      localStorage.removeItem("productId");
    } catch (error) {
      console.error('Error adding item:', error);
    }
  };

  const validationSchema = Yup.object().shape({
    
    productName: Yup.string().required("Product Name Required"),
    productWeight: Yup.string()
      .matches(/^[0-9]+$/, 'Product Weight should only contain digits')
      .matches(/^((?![eE]).)*$/, 'Letter "e" is not allowed')
      .required("Product Weight required"),
    productPrice: Yup.string()
      .matches(/^[0-9]+$/, 'Product Price should only contain digits')
      .matches(/^((?![eE]).)*$/, 'Letter "e" is not allowed')
      .required("Product Price required"),
    pNumber: Yup.string()
      .matches(/^[0-9]+$/, 'Enter only digits')
      .matches(/^((?![eE]).)*$/, 'Letter "e" is not allowed')
      .required("Please add At least one item"),
    
  });

  return (
    <div>
      <Formik
        initialValues={initialValues[0]} // Set initial values here
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ errors, touched, handleBlur, handleChange, setFieldValue }) => (
          <Form>
            <Box>
              <ThemeProvider theme={theme}>
              <Grid container component="main" >
               <CssBaseline />
               <Grid
                 item
                 xs={false}
                 sm={4}
                 md={4}
               />
               <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                 <Box
                   sx={{
                     my: 8,
                     mx: 4,
                     display: "flex",
                     flexDirection: "column",
                     alignItems: "center",
                   }}
                 >
                   <Avatar sx={{  bgcolor: "secondary.main" }}>
                <EditIcon/>
                   </Avatar>
                   <Typography component="h1" variant="h5"> 
                    <p>Add This Product to My Page</p>
                     </Typography>
                     <Grid container spacing={2}>
                     <Grid item xs={12} >
                     <Field name="productName">
                 {({ field }) => (
                   <TextField
                     {...field}
                     label="Product Name"
                     fullWidth
                     variant="standard"
                     disabled
                     error={Boolean(field.value && typeof field.value === 'string' && field.value.trim() === '' && field.touched)}
                    helperText={<ErrorMessage name="productName" />}
                   />
                 )}
               </Field> 
                     </Grid>
              <Grid item xs={12} >
                     <Field name="productWeight">
                   {({ field }) => (
                   <TextField
                     {...field}
                     fullWidth
                     label="Product Weight"
                     variant="standard"
                     disabled
                     error={Boolean(field.value && typeof field.value === 'string' && field.value.trim() === '' && field.touched)}
                     helperText={<ErrorMessage name="productWeight" />}
                   />
                 )}
               </Field> 
             </Grid>
             <Grid item xs={12} >
                     <Field name="productPrice">
                   {({ field }) => (
                   <TextField
                     {...field}
                     fullWidth
                     label="Product Price"
                     variant="standard"
                     disabled
                      error={Boolean(field.value && typeof field.value === 'string' && field.value.trim() === '' && field.touched)}
                     helperText={<ErrorMessage name="productPrice" />}
                   />
                 )}
               </Field> 
             </Grid>
             <Grid item xs={12} >
             <Field name="pNumber">
             {({ field, form }) => (
  <Select
    {...field}
    fullWidth
    label="Number of Products"
    variant="outlined"
    error={Boolean(field.value && typeof field.value === 'string' && field.value.trim() === '' && field.touched)}
    onChange={(event) => form.setFieldValue(field.name, event.target.value)} // Handle change
  >
    {[...Array(number)].map((_, index) => (
      <MenuItem key={index + 1} value={index + 1}>{index + 1}</MenuItem>
    ))}
  </Select>
)}

</Field>
              
             </Grid>
          
            <div>
              
            
             <Button
                       type="submit"
                       fullWidth
                       variant="contained"
                       sx={{ mt: 3, mb: 2 }}
                     >
                    Place Order
                     </Button> </div>
                      </Grid>
             </Box>
             </Grid>
             </Grid>
              </ThemeProvider>
            </Box>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Myproducts;

