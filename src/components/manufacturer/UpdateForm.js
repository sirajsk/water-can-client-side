

// UpdateForm.js
import * as React from "react";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom';
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import manufacturerService from "../../services/manufacturerService";
import productService from "../../services/productService";
import EditIcon from '@mui/icons-material/Edit';
const theme = createTheme();

const Updateform = ({ initialValues }) => {
  const navigate = useNavigate();

  const handleSubmit = async (values, actions) => {
    //const formData = new FormData();
   
 
    try {
      const res=productService.updateProduct(values)
      toast.success("Your Product Added Successfully!!! ");
      navigate('/manufacturer/listproducts');
      localStorage.removeItem("productId");
    } catch (error) {
      console.error('Error adding item:', error);
    }
  };

  const validationSchema = Yup.object().shape({
    
    productName: Yup.string().required("Product Name Required"),
    productWeight: Yup.string()
      .matches(/^[0-9]+$/, 'Product Weight should only contain digits')
      .matches(/^((?![eE]).)*$/, 'Letter "e" is not allowed')
      .required("Product Weight required"),
    productPrice: Yup.string()
      .matches(/^[0-9]+$/, 'Product Price should only contain digits')
      .matches(/^((?![eE]).)*$/, 'Letter "e" is not allowed')
      .required("Product Price required"),
    pNumber: Yup.string()
      .matches(/^[0-9]+$/, 'Enter only digits')
      .matches(/^((?![eE]).)*$/, 'Letter "e" is not allowed')
      .required("Please add At least one item"),
    
  });

  return (
    <div>
      <Formik
        initialValues={initialValues[0]} // Set initial values here
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ errors, touched, handleBlur, handleChange, setFieldValue }) => (
          <Form>
            <Box>
              <ThemeProvider theme={theme}>
              <Grid container component="main" >
               <CssBaseline />
               <Grid
                 item
                 xs={false}
                 sm={4}
                 md={4}
               />
               <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                 <Box
                   sx={{
                     my: 8,
                     mx: 4,
                     display: "flex",
                     flexDirection: "column",
                     alignItems: "center",
                   }}
                 >
                   <Avatar sx={{  bgcolor: "secondary.main" }}>
                <EditIcon/>
                   </Avatar>
                   <Typography component="h1" variant="h5"> 
                    <p>Update Product</p>
                     </Typography>
                     <Grid container spacing={2}>
                     <Grid item xs={12} >
                     <Field name="productName">
                 {({ field }) => (
                   <TextField
                     {...field}
                     label="Product Name"
                     fullWidth
                     variant="outlined"
                    // error={Boolean(field.value && field.value.trim() === '' && field.touched)}
                    error={Boolean(field.value && typeof field.value === 'string' && field.value.trim() === '' && field.touched)}
                    helperText={<ErrorMessage name="productName" />}
                   />
                 )}
               </Field> 
                     </Grid>
              <Grid item xs={12} >
                     <Field name="productWeight">
                   {({ field }) => (
                   <TextField
                     {...field}
                     fullWidth
                     label="Product Weight"
                     variant="outlined"
                     error={Boolean(field.value && typeof field.value === 'string' && field.value.trim() === '' && field.touched)}
                     //error={Boolean(field.value && field.value.trim() === '' && field.touched)}
                     helperText={<ErrorMessage name="productWeight" />}
                   />
                 )}
               </Field> 
             </Grid>
             <Grid item xs={12} >
                     <Field name="productPrice">
                   {({ field }) => (
                   <TextField
                     {...field}
                     fullWidth
                     label="Product Price"
                     variant="outlined"
                     error={Boolean(field.value && typeof field.value === 'string' && field.value.trim() === '' && field.touched)}
                    // error={Boolean(field.value && field.value.trim() === '' && field.touched)}
                     helperText={<ErrorMessage name="productPrice" />}
                   />
                 )}
               </Field> 
             </Grid>
             <Grid item xs={12} >
                     <Field name="pNumber">
                   {({ field }) => (
                   <TextField
                     {...field}
                     fullWidth
                     label="Number of Products"
                     variant="outlined"
                     error={Boolean(field.value && typeof field.value === 'string' && field.value.trim() === '' && field.touched)}
                    // error={Boolean(field.value && field.value.trim() === '' && field.touched)}
                     helperText={<ErrorMessage name="pNumber" />}
                   />
                 )}
               </Field> 
             </Grid>
           {/* <Grid item xs={12}>
           <Field name="image">
                 {({ field }) => (
                   <TextField
                     type="file"
                     //label="Image"
                     variant="outlined"
                     error={Boolean(!field.value && field.touched)}
                     helperText={<ErrorMessage name="image" />}
                     onChange={(event) => setFieldValue("image", event.currentTarget.files[0])}
                   />
                 )}
               </Field>
          </Grid> */}
            <div>
              
            
             <Button
                       type="submit"
                       fullWidth
                       variant="contained"
                       sx={{ mt: 3, mb: 2 }}
                     >
                     Update
                     </Button> </div>
                      </Grid>
             </Box>
             </Grid>
             </Grid>
              </ThemeProvider>
            </Box>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Updateform;

