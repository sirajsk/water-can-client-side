import React, { useState, useEffect } from 'react';
import { RadioGroup, FormControlLabel, Radio } from '@mui/material';

const Addresscomponent = ({ addresses }) => {
  const [selectedAddress, setSelectedAddress] = useState('');

  useEffect(() => {
    if (addresses.length > 0) {
      setSelectedAddress(addresses[0]._id.toString()); // Set the default value when addresses change
      localStorage.setItem("addressId",addresses[0]._id.toString());
    }
  }, [addresses]);

  const handleAddressChange = (event) => {
    setSelectedAddress(event.target.value); // Update the selected address when changed
    localStorage.removeItem("addressId")
    const addressId=event.target.value;
    localStorage.setItem("addressId",addressId);
  
  };

  return (
    <RadioGroup value={selectedAddress} onChange={handleAddressChange}>
      {addresses.map((address) =>
        address && address._id ? (
          <FormControlLabel
            key={address._id}
            value={address._id.toString()}
            control={<Radio />} 
            label={`${address.name}, ${address.addressLine1}, ${address.city}, ${address.state}, ${address.postalCode}, ${address.country}`}
          />
        ) : null
      )}
    </RadioGroup>
  );
};

export default Addresscomponent;
