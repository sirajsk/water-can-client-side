import React from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Button from '@mui/material/Button';

const PaymentForm = ({ handleProceed }) => {
  return (
    <Formik
      initialValues={{ payment: 'cashondelivery' }} // Set initial value to 'cashondelivery'
      validationSchema={Yup.object({
        payment: Yup.string().required('Payment method is required'),
      })}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }, 400);
      }}
    >
      {({ isSubmitting, isValid, validateForm }) => (
        <Form>
          <div>
            <Field name="payment">
              {({ field, form }) => (
                <FormControl fullWidth>
                  <InputLabel id="payment"></InputLabel>
                  <Select
                    {...field}
                    labelId="payment"
                    id="payment"
                    label="Payment"
                    error={form.errors.payment && form.touched.payment}
                    helperText={form.errors.payment}
                  >
                    <MenuItem value="cashondelivery">Cash On Delivery</MenuItem>
                    <MenuItem value="card">Card</MenuItem>
                    <MenuItem value="upi">UPI</MenuItem>
                  </Select>
                </FormControl>
              )}
            </Field>
          </div>
        
        </Form>
      )}
    </Formik>
  );
};

export default PaymentForm;
