import axios from 'axios';
const API_URL = 'http://localhost:3001/api/cart'; 

const cartService = {
  addToCart:async(formData)=>{
    try{
      const response = axios.post(`${API_URL}/addcart`,formData);
      return response;
    }catch(error){
      console.error('Error fetching user:', error);
      throw error;
    }
  },
    
    getItemQuantityInCart:async(userId, productId,vendorId)=>{
        try{ 
         const response = await fetch(`${API_URL}/getquantity/${userId}/${productId}/${vendorId}`);
        
         return response
            }catch(error){
          console.error('Error fetching user:', error); 
          throw error;
        }
      },
   

      updateCart:async(formData)=>{
        console.log("form = "+formData)
    try{
      const response = axios.put(`${API_URL}/updateCart`,formData);
      return response;
    }catch(error){
      console.error('Error fetching user:', error);
      throw error;
    }
  },

  getCart:async(id,vendorId)=>{
    try{
       
         const response =await fetch(`${API_URL}/cart/${id}/${vendorId}`);
        return response
      
    }catch(error){
      console.error('Error fetching user:', error);
      throw error;
    }
  },

  
  removeItem:async(id,vendorId)=>{
    try{
      const response = axios.put(`${API_URL}/remove/${id}/${vendorId}`);
      return response;
    }catch(error){
      console.error('Error fetching user:', error);
      throw error;
    }
  },

  

}

export default cartService