
import axios from 'axios';
const API_URL = 'http://localhost:3001/api/vendor'; 

const vendorService = {
           
    getProductsbyId:async(id)=>{
        try{
           
             const response =await fetch(`${API_URL}/productsbyvendor/${id}`);
             return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      }, 
      getQuantityById:async(id,vendorId)=>{
        try{
           
             const response =await fetch(`${API_URL}/getquantitybyid/${id}/${vendorId}`);
            // console.log("respo in service= "+response.productName)
            return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },

      getItemQuantityInCart:async(userId, productId,vendorId)=>{
        try{ 
         const response = await fetch(`${API_URL}/getquantity/${userId}/${productId}/${vendorId}`);
        
         return response
            }catch(error){
          console.error('Error fetching user:', error); 
          throw error;
        }
      },
      getOrders:async(id)=>{
        try{
           
             const response =await fetch(`${API_URL}/orders/${id}`);
            return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },

      placeOrder:async(data)=>{
        try{
       
          const response = axios.post(`${API_URL}/placeorder`,data);
          return response;
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },
   
}

export default vendorService